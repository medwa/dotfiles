let mapleader=" "

" Plugins
call plug#begin('~/.config/nvim/plugged')
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdtree'
Plug 'junegunn/goyo.vim'
Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'bling/vim-airline'
Plug 'reedes/vim-pencil'
Plug 'vim-airline/vim-airline-themes'
Plug 'Lokaltog/vim-monotone'
call plug#end()

" Theme
" colorscheme monotone
hi Normal guibg=NONE ctermbg=NONE
set termguicolors
let g:monotone_contrast_factor=1.1
let g:monotone_emphasize_comments=1
let g:monotone_secondary_hue_offset=200
let g:airline_theme='monochrome'

" Basic Definitions
filetype plugin on
set nocompatible
set clipboard+=unnamedplus
set hlsearch
set number relativenumber
set encoding=utf-8
set fileencoding=utf-8
syntax on
set noswapfile

" Buffer navigation
set splitbelow splitright
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Formatting
set shiftwidth=1
set autoindent
set smartindent

autocmd BufWritePre * %s/\s\+$//e
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

set wildmode=longest,list,full
nnoremap S :%s//g<Left><left>
map <leader>o :setlocal spell! spelllang=en_us<CR>

set virtualedit=onemore

" NERDTree
let NERDTreeShowHidden=1
map <leader>n :NERDTreeToggle<CR>

" vim-pencil
map <leader>p :TogglePencil<CR>

" Goyo
function! s:goyo_enter()
  let b:quitting = 0
  let b:quitting_bang = 0
  autocmd QuitPre <buffer> let b:quitting = 1
  cabbrev <buffer> q! let b:quitting_bang = 1 <bar> q!
endfunction

function! s:goyo_leave()
  " Quit Vim if this is the only remaining buffer
  if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
    if b:quitting_bang
      qa!
    else
      qa
    endif
  endif
endfunction

autocmd! User GoyoEnter call <SID>goyo_enter()
autocmd! User GoyoLeave call <SID>goyo_leave()
map <leader>g :Goyo<CR>
