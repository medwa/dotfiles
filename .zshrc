# Display and colors
autoload colors && colors
for COLOR in RED GREEN YELLOW BLUE MAGENTA CYAN BLACK WHITE; do
	eval $COLOR='%{$fg_no_bold[${(L)COLOR}]%}'
	eval BOLD_$COLOR='%{$fg_bold[${(L)COLOR}]%}'
done
PS1="%B%{$fg[magenta]%}[%{$fg[cyan]%}%~%{$fg[magenta]%}]%{$reset_color%}%b "

# General settings
setopt hist_ignore_dups

#export GDK_DPI_SCALE="1.25"
#export QT_SCALE_FACTOR="1.25"

# Default programs
export EDITOR="vim"
#export TERMINAL="alacritty"
#export BROWSER="chromium"
export READER="zathura"
export FILE="ranger"
export STATUSBAR="i3blocks"

# Directories
export PATH="$PATH:/$HOME/.local/bin/"
#export SUDO_ASKPASS="$HOME/.local/bin/bemenupass"

# Autocomplete
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# Tab-complete vim bindings
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -v '^?' backward-delete-char
export KEYTIMEOUT=1

# Start i3 in tty1 (leaf)
if [ "$(tty)" = "/dev/tty1" ] && [ "$(uname -n)" = "leaf" ]; then
 exec sway
fi

# Aliases
alias psearch='yay -Slq | fzf --multi --preview 'yay -Si {1}' | xargs -ro yay -S'
alias orphans="yay -Qtdq | yay -Rns -"
alias plog="cat /var/log/pacman.log"

alias .="cd ../"
alias ..="cd ../../"
alias ...="cd ../../../"
alias ....="cd ../../../../"
alias mine="sudo chown -R $(whoami):wheel"
alias enc="openssl aes-256-cbc -a -salt -iter 5 -in"
alias dec="openssl aes-256-cbc -d -a -salt -iter 5 -in"

alias sway="dbus-launch --sh-syntax --exit-with-session /usr/bin/sway"
alias yt="yt-dlp --add-metadata -i -o '%(upload_date>%Y-%m-%d)s - %(title)s.%(ext)s'"
alias ytv="yt -S '+codec:h264'"
alias yta="yt -x -f bestaudio/best"
alias ytmp3="yt-dlp -i -x --audio-format mp3"
alias flacmp3='for i in *.flac; do ffmpeg -i "$i" "${i%.*}.mp3"; done'
alias mp4mp3='for i in *.mp4; do ffmpeg -i "$i" "${i%.*}.mp3"; done'
alias nvim="vim"
alias androidmount="jmtpfs ~/mnt"
alias androidumount="sudo umount -R ~/mnt"
alias ovpn_us="cd /etc/openvpn && sudo openvpn --config /etc/openvpn/mullvad_us_all.conf"
alias ovpn="cd /etc/openvpn && sudo openvpn --config"
alias tk="tmux kill-server"
alias hugodfr="hugo serve --disableFastRender"
