set nocompatible

let mapleader=" "
filetype plugin on
set clipboard^=unnamed,unnamedplus
set hlsearch
set number relativenumber
set encoding=utf-8
set fileencoding=utf-8
syntax on
set noswapfile
set laststatus=2

set splitbelow splitright
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

set shiftwidth=1
set autoindent
set smartindent

autocmd BufWritePre * %s/\s\+$//e
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

set wildmode=longest,list,full
nnoremap S :%s//g<Left><left>
set virtualedit=onemore
